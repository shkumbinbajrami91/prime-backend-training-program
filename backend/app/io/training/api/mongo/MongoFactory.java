package io.training.api.mongo;

import akka.actor.CoordinatedShutdown;
import com.typesafe.config.Config;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class MongoFactory {

	private CoordinatedShutdown coordinatedShutdown;
	private Config configuration;
	private static UserMongo global;
	private static TestMongo test;

	@Inject
	public MongoFactory(CoordinatedShutdown coordinatedShutdown, Config config) {
		this.configuration = config;
		this.coordinatedShutdown = coordinatedShutdown;
	}

	public MongoDB create() {
		String mode = configuration.getString("mode");
		if (mode.equalsIgnoreCase("test")) {
			return this.getTestMongo();
		}
		return getGlobalMongo();
	}

	private UserMongo getGlobalMongo() {
		if (global == null) {
			global = new UserMongo(coordinatedShutdown, configuration);
		}
		return global;
	}

	private TestMongo getTestMongo() {
		if (test == null) {
			test = new TestMongo(coordinatedShutdown, configuration);
		}
		return test;
	}
}
