package io.training.api.controllers;

import com.google.inject.Inject;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletableFuture;

public class ApplicationController extends Controller {

	@Inject
	HttpExecutionContext ec;

	public CompletableFuture<Result> healthCheck() {
		return CompletableFuture.supplyAsync(Results::ok, ec.current());
	}
}