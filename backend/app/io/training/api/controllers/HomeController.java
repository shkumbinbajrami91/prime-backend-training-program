package io.training.api.controllers;

import com.google.inject.Inject;
import com.mongodb.client.MongoCollection;
import com.typesafe.config.Config;
import io.training.api.mongo.MongoFactory;
import org.bson.Document;
import play.Logger;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletableFuture;

public class HomeController extends Controller {

	@Inject
	Config config;

	@Inject
	HttpExecutionContext ec;

	public CompletableFuture<Result> index() {
		return CompletableFuture.supplyAsync(() -> ok(views.html.home.render(config)), ec.current());
	}

	public CompletableFuture<Result> indexAt(String path) {
		return index();
	}
}