import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import play.Application;
import play.Logger;
import play.mvc.Http;
import play.test.Helpers;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;
import play.mvc.Result;

public class ApplicationTestCases {
	protected static Application app;

	@BeforeClass
	public static void startPlay() {
		Logger.of("testing").info("Starting {} Test Cases", ApplicationTestCases.class.getSimpleName());
		app = Helpers.fakeApplication();
		Helpers.start(app);
	}

	@AfterClass
	public static void stopPlay() {
		Logger.of("testing").info("Finished {} Test Cases", ApplicationTestCases.class.getSimpleName());
		if (app != null) {
			Helpers.stop(app);
			app = null;
		}
	}

	@Test
	public void testIndex() {
		Http.RequestBuilder homeRequest = new Http.RequestBuilder().method("GET").uri("/");
		Result result = route(app, homeRequest);
		assertEquals(OK, result.status());
		assertEquals("text/html", result.contentType().get());
		assertEquals("utf-8", result.charset().get());
		assertTrue(contentAsString(result).contains("Welcome to your Play Web Application!"));
	}
}
